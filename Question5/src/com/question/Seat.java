package com.question;

public class Seat {

	private int seatNo;
	private boolean isOccupied;
	
	public Seat() {
		// TODO Auto-generated constructor stub
	}

	public Seat(int seatNo, boolean isOccupied) {
		super();
		this.seatNo = seatNo;
		this.isOccupied = isOccupied;
	}

	public int getSeatNo() {
		return seatNo;
	}

	public void setSeatNo(int seatNo) {
		this.seatNo = seatNo;
	}

	public boolean isOccupied() {
		return isOccupied;
	}

	public void setOccupied(boolean isOccupied) {
		this.isOccupied = isOccupied;
	}

	@Override
	public String toString() {
		return "Seat [seatNo=" + seatNo + ", isOccupied=" + isOccupied + "]";
	}
	
	
}
