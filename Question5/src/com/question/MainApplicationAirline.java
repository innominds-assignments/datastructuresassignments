package com.question;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Class with Main method providing menu driven interface for user to book tickets
 * @author vsahu
 *
 */
public class MainApplicationAirline {
	public static void main(String[] args) {

		Seat[] allSeats = new Seat[10];
		Map<String, Seat> seatMap = new HashMap<String, Seat>();
		int seatNo = 1;
		for (int i = 0; i < 10; i++) {
			allSeats[i] = new Seat(seatNo, false);
			seatNo++;
		}

		try (Scanner sc = new Scanner(System.in)) {
			boolean continued = true;
			String input;

			while (continued) {
				System.out.println("Enter your choice : ");
				System.out.println("1. Enter 1 For Smoking Section");
				System.out.println("2. Enter 2 For Non Smoking Section");
				int choice = sc.nextInt();

				switch (choice) {
				case 1: // Book Tickets For Smoking Section
					seatMap = AirlineReservationSystem.bookSeatsSmokingSection(allSeats);
					if (seatMap.keySet().toString().equals("[Seat Booked]"))
						AirlineReservationSystem.printBoardingPass(seatMap);
					System.out.println(seatMap.keySet().toString());
					if (seatMap.containsKey("Seat Full")) {
						System.out.println("Smoking Section is full.. Do you want to book seat in Non Smoking Section");
						System.out.println("Enter Yes or No");
						input = sc.next();
						if (input.equalsIgnoreCase("yes")) {
							seatMap = AirlineReservationSystem.bookSeatsNonSmokingSection(allSeats);
							if (seatMap.keySet().toString().equals("[Seat Booked]"))
								AirlineReservationSystem.printBoardingPass(seatMap);
						} else
							System.out.println("Next flight leaves in 3 hours.");
					}
					break;

				case 2: // Book Tickets For Non Smoking Section
					seatMap = AirlineReservationSystem.bookSeatsNonSmokingSection(allSeats);
					if (seatMap.keySet().toString().equals("[Seat Booked]"))
						AirlineReservationSystem.printBoardingPass(seatMap);
					System.out.println(seatMap.keySet().toString());
					if (seatMap.containsKey("Seat Full")) {
						System.out.println("Non Smoking Section is full.. Do you want to book seat in Smoking Section");
						System.out.println("Enter Yes or No");
						input = sc.next();
						if (input.equalsIgnoreCase("yes"))
							seatMap = AirlineReservationSystem.bookSeatsSmokingSection(allSeats);
							if (seatMap.keySet().toString().equals("[Seat Booked]"))
								AirlineReservationSystem.printBoardingPass(seatMap);
						else
							System.out.println("Next flight leaves in 3 hours.");

					}
					break;

				case 3:
					System.out.println(seatMap);
					continued = false;
					break;
				}
			}
		}
	}
}
