package com.question;

import java.util.Collections;
import java.util.Map;

public class AirlineReservationSystem {
	
	

	/**
	 * Method to book Seats in Smoking Section
	 * @param seats Array of Seats in the plane.
	 * @return A Map consisting the status whether the seat is booked or not along with the booked seat and null in case if not booked.
	 */
	public static Map<String, Seat> bookSeatsSmokingSection(Seat[] seats) {

		boolean status = true;
		boolean flag = false;
		Seat bookedSeat = new Seat();
		while (status) {
			for (int j = 0; j < 5; j++) {
				if (seats[j].isOccupied() == false) {
					seats[j].setOccupied(true);
					bookedSeat = seats[j];
					flag = true;
					break;
				}
			}
			status = false;
		}
		if (flag == true)
			return Collections.singletonMap("Seat Booked", bookedSeat);

		else
			return Collections.singletonMap("Seat Full", null);
	}
	/**
	 * Method to book Seats in Non Smoking Section
	 * @param seats Array of Seats in the plane.
	 * @return A Map consisting the status whether the seat is booked or not along with the booked seat and null in case if not booked.
	 */
	public static Map<String, Seat> bookSeatsNonSmokingSection(Seat[] seats) {

		boolean status = true;
		boolean flag = false;
		Seat bookedSeat = new Seat();
		while (status) {
			for (int j = 5; j < 10; j++) {
				if (seats[j].isOccupied() == false) {
					seats[j].setOccupied(true);
					bookedSeat = seats[j];
					flag = true;
					break;
				}
			}
			status = false;

		}
		if (flag == true)
			return Collections.singletonMap("Seat Booked", bookedSeat);

		else
			return Collections.singletonMap("Seat Full", null);
	}
	
	/**
	 * Method to print the Boarding Pass
	 * @param seatMap having the details of the booked seat
	 */
	public static void printBoardingPass(Map<String, Seat> seatMap) {
		System.out.println("----------------------------------------------------------------------------------------------------");
		System.out.println("****Boarding Pass****");
		System.out.println("****Java Airways****");
		Seat seat = seatMap.get("Seat Booked");
		System.out.println("Seat Number : " +seat.getSeatNo());
		if(seat.getSeatNo()<6)
			System.out.println("Section : Smoking Section");
		else
			System.out.println("Section : Non Smoking Section");
		System.out.println("----------------------------------------------------------------------------------------------------");
	}
}
