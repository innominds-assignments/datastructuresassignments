package com.question;

import java.util.Scanner;

public class ReservationSystem {

	private static Seat[] allSeats;

	public static Seat[] assignSeatingCapacity(int seatingCapacity) {
		allSeats = new Seat[seatingCapacity];
		int seatNo = 1;
		for (int i = 0; i < seatingCapacity; i++) {
			allSeats[i] = new Seat(seatNo, false);
			seatNo++;
		}

		return allSeats;
	}

	public static Seat[] bookSeat(Seat[] seats) {
		boolean status = true;
		while (status) {		
		for(int j = 0 ; j < seats.length; j++) {
			if(seats[j].isOccupied() == false) {
				seats[j].setOccupied(true);
				break;
			}
		}
		status = false;
		}
		
		return seats;
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		Seat[] seats = assignSeatingCapacity(sc.nextInt());
		System.out.println(seats[0]);
		System.out.println(seats[1]);
		System.out.println(seats[2]);
		seats = bookSeat(seats);
	
		System.out.println(seats[0]);
		seats = bookSeat(seats);
		System.out.println(seats[1]);
		System.out.println(seats[2]);
		
	}
}
