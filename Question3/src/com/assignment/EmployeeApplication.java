package com.assignment;

import java.util.Arrays;
import java.util.Scanner;

public class EmployeeApplication {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			EmployeeDb employeedb = new EmployeeDb();

			Employee e1 = new Employee(1, "Vaibhav", "vaibhav@test.com", 'M', 60000);
			Employee e2 = new Employee(2, "Kunal", "Kunal@test.com", 'M', 70000);
			Employee e3 = new Employee(3, "Test", "test@test.com", 'M', 80000);
			employeedb.addEmployee(e1);
			employeedb.addEmployee(e2);
			employeedb.addEmployee(e3);
			
			boolean exit = false;
			while(!exit) {
			
				System.out.println("*****Enter the choice******");
				System.out.println("1. Add Employee");
				System.out.println("2. Delete Employee");
				System.out.println("3. Show payslip of Employee");
				System.out.println("4. List All Employee");
				System.out.println("5. Exit");
				
				int choice = sc.nextInt();
				switch(choice) {
				case 1:
					System.out.println("Enter empId, name, email, gender and salary");
					Employee newEmployee = new Employee(sc.nextInt(), sc.next(), sc.next(), sc.next().charAt(0), sc.nextFloat());
					boolean added = employeedb.addEmployee(newEmployee);
					if(added == true)
						System.out.println("employee added");
					break;
					
				case 2:
					System.out.println("Enter Employee Id");
					int empId = sc.nextInt();
					boolean deleted = employeedb.deleteEmployee(empId);
					break;
				
				case 3:
					System.out.println("Enter Employee Id");
					System.out.println(employeedb.showPaySlip(sc.nextInt()));
					break;
			
				case 4:
					System.out.println(Arrays.toString(employeedb.listAll()));
					break;
					
				case 5:
					exit = true;
					break;
				}
			}
		}
}
}
