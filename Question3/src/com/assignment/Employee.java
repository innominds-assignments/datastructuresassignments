package com.assignment;

public class Employee implements Comparable<Employee> {
	private int empId;
	private String empName;
	private String empEmail;
	private char empGender;
	private float empSalary;

	public Employee() {
		super();
	}

	public Employee(int empId, String empName, String empEmail, char empGender, float empSalary) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empEmail = empEmail;
		this.empGender = empGender;
		this.empSalary = empSalary;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public float getEmpSalary() {
		return empSalary;
	}

	public void setEmpSalary(float empSalary) {
		this.empSalary = empSalary;
	}

	@Override
	public boolean equals(Object obj) {
		return ((Integer) ((Employee) obj).empId).equals(this.empId);
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", empEmail=" + empEmail + ", empGender="
				+ empGender + ", empSalary=" + empSalary + "]";
	}

	public void GetEmployeeDetails() {
		System.out.println("Employee [empId=" + this.empId + ", empName=" + this.empName + ", empEmail=" + this.empEmail
				+ ", empGender=" + this.empGender + ", empSalary=" + this.empSalary + "]");
	}

	@Override
	public int compareTo(Employee o) {
		return ((Integer) (this.getEmpId())).compareTo((Integer) o.empId);
	}

}
