package com.assignment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

public class EmployeeDb {
	
	ArrayList<Employee> employeeList = new ArrayList<>();
	TreeSet<Employee> employeeSet = new TreeSet<>();

	public boolean addEmployee(Employee e) { 
		if(employeeList.add(e) && employeeSet.add(e) == true)
			return true;
		return false;
	}

	public boolean deleteEmployee(int empId) { 
		Iterator<Employee> itr = employeeList.iterator();
		while (itr.hasNext()) {
			Employee emp = itr.next();
			if (emp.getEmpId() == empId) {
				employeeList.remove(emp);
				employeeSet.remove(emp);
				return true;
			}
		}
		return false;
	}

	public String showPaySlip(int empId) { 
		Iterator<Employee> itr = employeeSet.iterator();
		while (itr.hasNext()) {
			Employee emp = itr.next();
			if (emp.getEmpId() == empId) {
				return emp.getEmpSalary()+"";
			}
		}
		return "Employee " + empId + " is not present or wrong employee Id";
	}

	public Employee[] listAll() {
		Employee[] emp = new Employee[employeeSet.size()];
		int i = 0;
		for (Employee employee : employeeSet) {
			emp[i] = employee;
			i++;
		}
		
		return emp;
	}

}
