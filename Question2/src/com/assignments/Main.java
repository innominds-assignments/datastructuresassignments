package com.assignments;

import java.util.LinkedList;
import java.util.Scanner;


public class Main {
	public static void main(String[] args) {
		
	
	try (Scanner sc = new Scanner(System.in)) {
		Question2 ques = new Question2();

		LinkedList<Integer> a1 = new LinkedList<>();
		LinkedList<Integer> a2 = new LinkedList<>();

		System.out.println("Enter the upper limit : ");
		int n = sc.nextInt();

		a1 = ques.saveEvenNumbers(n);
		System.out.print("prime Numbers => ");
		for (int i : a1) {
			System.out.print(i + ", ");
		}
		System.out.println();

		a2 = ques.printEvenNumbers(a1);
		System.out.print("doubled prime Numbers => ");
		for (int i : a2) {
			System.out.print(i + ", ");
		}

		System.out.println();
		System.out.println("Enter the number to be Searched ");
		int number = ques.printEvenNumber(sc.nextInt());
		System.out.println(number);

	}

	}
}
