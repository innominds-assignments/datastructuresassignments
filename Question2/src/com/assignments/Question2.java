package com.assignments;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Develop a java class with a method saveEvenNumbers(int N) using ArrayList to
 * store even numbers from 2 to N, where N is a integer which is passed as a
 * parameter to the method saveEvenNumbers(). The method should return the
 * ArrayList (A1) created. In the same class create a method printEvenNumbers()
 * which iterates through the arrayList A1 in step 1, and It should multiply
 * each number with 2 and display it in format 4,8,12�.2*N. and add these
 * numbers in a new ArrayList (A2). The new ArrayList (A2) created needs to be
 * returned. Create a method printEvenNumber(int N) parameter is a number N.
 * This method should search the arrayList (A1) for the existence of the number
 * �N� passed. If exists it should return the Number else return zero.
 * 
 * @author Vaibhav Sahu
 * @category assignment
 * 
 */
public class Question2 {

	LinkedList<Integer> integersListA1 = new LinkedList<Integer>();

	LinkedList<Integer> integersListA2 = new LinkedList<Integer>();

	/**
	 * Method to save the Even Numbers in the LinkedList
	 * 
	 * @param n is the upper limit upto which even numbers are required
	 * @return LinkedList containing even numbers from 2 to n.
	 */
	public LinkedList<Integer> saveEvenNumbers(int n) {
		for (int i = 2; i <= n; i++) {
			if (i == n && i % 2 == 0)
				integersListA1.add(i);
			else if (i % 2 == 0)
				integersListA1.add(i);
		}
		return integersListA1;
	}

	/**
	 * method printEvenNumbers() which iterates through the LinkedList A1 in step 1,
	 * and multiply each number with 2 and display it in format 4,8,12�.2*N. and add
	 * these numbers in a new LinkedList (A2)
	 * 
	 * @param a1 list containing even numbers
	 * @return list consisting of the even numbers multiplied by 2
	 */
	public LinkedList<Integer> printEvenNumbers(List<Integer> a1) {
		integersListA2 = integersListA1.stream().map(i -> i * 2).collect(Collectors.toCollection(LinkedList::new));

		return integersListA2;
	}
	
	/**
	 * Method printEvenNumber(int N) parameter is a number N. This method should
	 * search the arrayList (A1) for the existence of the number �N� passed. If
	 * exists it should return the Number else return zero.
	 * 
	 * @param n Number to be searched
	 * @return the number else 0.
	 */
	public int printEvenNumber(int n) {
		if (integersListA1.contains(n))
			return n;
		else
			return 0;
	}
}
