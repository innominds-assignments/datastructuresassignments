package com.assignment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class WTIApplication {

	public static void main(String[] args) {

		try (Scanner sc = new Scanner(System.in)) {

			List<Employee> allEmployees = new ArrayList<Employee>();
			allEmployees.add(new Employee(1, "Vaibhav", "Sahu", "vai@test.com"));
			allEmployees.add(new Employee(2, "Kunal", "Asnani", "ka@test.com"));
			allEmployees.add(new Employee(3, "Shubham", "Hore", "sh@test.com"));
			allEmployees.add(new Employee(4, "Kunal", "Jai", "kj@test.com"));
			allEmployees.add(new Employee(5, "Adesh", "kes", "ak@test.com"));
			allEmployees.add(new Employee(6, "Sourabh", "Kale", "sk@test.com"));
			allEmployees.add(new Employee(7, "Amit", "Anwade", "aa@test.com"));
			allEmployees.add(new Employee(8, "Bhushan", "Raut", "br@test.com"));

			boolean exit = false;
			while (!exit) {

				System.out.println("*****Enter the choice******");
				System.out.println("1. Choose a random employee");
				System.out.println("2. Get List of Unique First Names");
				System.out.println("3. Count the number of Employees with Each Name");
				System.out.println("4. To add Employee to the Waiting Ticket List");
				System.out.println("5. To remove Employee from the Waiting Ticket List");
				System.out.println("6. Exit");

				int choice = sc.nextInt();
				switch (choice) {
				case 1:
					Employee emp = EmployeeUtils.getRandomEmployeeForFreeToy(allEmployees);
					System.out.println(emp);
					break;

				case 2:
					List<String> uniqueNames = EmployeeUtils.getUniqueName(allEmployees);
					System.out.println(uniqueNames);
					break;

				case 3:
					Map<String, Integer> mostPopularNames = EmployeeUtils.mostPopularNames(allEmployees);
					for (Map.Entry<String, Integer> entry : mostPopularNames.entrySet()) {
						System.out.println(
								"Name : " + entry.getKey() + " No. of Employees with This name : " + entry.getValue());
					}
					break;

				case 4:
					System.out.println("Enter empId, firstName, LastName, Email");
					Employee newEmployee = new Employee(sc.nextInt(), sc.next(), sc.next(), sc.next());
					System.out.println(EmployeeUtils.addEmployeeToSeasonTicketList(allEmployees, newEmployee));
					allEmployees.add(newEmployee);
					break;

				case 5:
					System.out.println("Enter empId of the employee to be removed : ");

					Employee emp1 = EmployeeUtils.findByEmpId(allEmployees, sc.nextInt());
					System.out.println(EmployeeUtils.removeEmployeeFromSeasonTicketList(allEmployees, emp1));
					allEmployees.remove(emp1);
					break;

				case 6:
					exit = true;
					break;
				}
			}
		}
	}

}
