package com.assignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class EmployeeUtils {

	public static Employee getRandomEmployeeForFreeToy(List<Employee> allEmployees) {
		
		Employee emp = new Employee();
		Random random = new Random();

		int randomIndex = random.nextInt(allEmployees.size());
		emp = allEmployees.get(randomIndex);

		return emp;
	}

	public static List<String> getUniqueName(List<Employee> allEmployees) {
		List<String> namesList = new ArrayList<String>();

		allEmployees.forEach(employee -> {
			namesList.add(employee.getFirstName());
		});

		Set<String> set = new HashSet<String>();
		namesList.forEach(name -> {
			set.add(name);
		});

		List<String> uniqueFirstNames = new ArrayList<String>();
		uniqueFirstNames = set.stream().collect(Collectors.toList());

		return uniqueFirstNames;

	}

	public static Map<String, Integer> mostPopularNames(List<Employee> allEmployees) {
	
		Map<String, Integer> namesMap = new HashMap<String, Integer>();

		allEmployees.forEach(emp -> {
			if (namesMap.containsKey(emp.getFirstName())) {
				namesMap.put(emp.getFirstName(), namesMap.get(emp.getFirstName()) + 1);
			} else
				namesMap.put(emp.getFirstName(), 1);
		});

		return namesMap;
	}

	public static Queue<Employee> seasonTickets(List<Employee> allEmployees) {
		Queue<Employee> waitingList = new LinkedList<Employee>();
		allEmployees.forEach(emp-> {
			waitingList.add(emp);
		});
		return waitingList;

	}

	public static String addEmployeeToSeasonTicketList(List<Employee> allEmployees,Employee emp) {
		boolean result = seasonTickets(allEmployees).add(emp);
		if (result)
			return "employeeAdded";
		else
			return "Not Added";
	}

	public static String removeEmployeeFromSeasonTicketList(List<Employee> allEmployees,Employee emp) {
		boolean result = seasonTickets(allEmployees).remove(emp);
		if (result)
			return "employee removed";
		else
			return "Not Removed";
	}

	public static Employee findByEmpId(List<Employee> allEmployees, int empId) {
		for(Employee e : allEmployees) {
			if(e.getId() == empId)
				return e;
		}
		return null;
	}
}
